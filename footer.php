	
		

		<footer id="footer" class="container mx-auto mt-2 p-4 flex justify-between content-center items-center" >
				
				<span>
				&copy; <?php echo esc_html( date_i18n( __( 'Y', 'blankslate' ) ) ); ?> <?php echo esc_html( get_bloginfo( 'name' ) ); ?>
				</span>
				<a class=" w-40 align-right" href="https://www.montessoricanela.es/"> <img src="<?php echo get_template_directory_uri(); ?>/img/canela-montessori.png" alt=""></a>

				<span class="ludus w-16 align-right">
					<a href="https://ludus.org.es/es/escola-natura"><img src="https://ludus.org.es/assets/supporter-c82696f0fe97a32c10723b47ea4a6026.png" alt=""></a>
					<!-- <script type="application/javascript" src="https://ludus.org.es/ca/supporter_banner/2147.js?v=1"></script> -->
				</span>
		

		</footer>

		
	<style>

	.ludus a {
		float:right;
	}
	
	</style>

	<script>
		
		var scene = document.getElementById('scene');
		var parallaxInstance = new Parallax(scene);

	</script>

	<?php wp_footer(); ?>
</body>
</html>