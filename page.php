<?php get_header(); ?>

<div class="container mx-auto max-w-2xl  px-4 md:px-8 xl:px-40"  style="min-height: 60vh">


	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<header class="header">
			<h1 class="text-center text-5xl mb-10 tracking-tight font-bold "><?php the_title(); ?></h1> 
		</header>
		
		<div class="entry-content">
			<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
			<?php the_content(); ?>
			<div class="entry-links"><?php wp_link_pages(); ?></div>
		</div>
	
	</article>
	
	<?php if ( comments_open() && ! post_password_required() ) { comments_template( '', true ); } ?>
	<?php endwhile; endif; ?>

</div>


<?php edit_post_link(); ?>


<?php //get_sidebar(); ?>
<?php get_footer(); ?>