<?php get_header(); ?>


<main class="container mx-auto px-4 md:w-1/2 " >
    
    <header class="header text-center text-5xl mb-10 tracking-tight font-bold ">
        <h1 class="entry-title"><?php single_term_title(); ?></h1>
    </header>
    
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
    <div class="mb-10">
        
        <h2 class="font-bold text-3xl "><?php the_title()?></h2>
        <h3 class="mb-5"><?php the_field('cargo')?></h3>
        <div class="mb-5"><?php the_post_thumbnail()?></div>
        
        <div><?php the_content()?></div>
    
    </div>
    
    <?php endwhile; endif; ?>
   
    <?php //get_template_part( 'nav', 'below' ); ?>

</main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>