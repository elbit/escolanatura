var postcss = require('gulp-postcss');
var gulp = require('gulp');
var autoprefixer = require('autoprefixer');
var browserSync = require('browser-sync').create();


gulp.task('css', function () {
  var postcss = require('gulp-postcss');
  var tailwindcss = require('tailwindcss');

  return gulp.src('style.css')
    // ...
    .pipe(postcss([
      // ...
      tailwindcss('tailwind.config.js'),
      require('autoprefixer'),
      // ...
    ]))
    // ...
    .pipe(gulp.dest('build/'));
})





// use default task to launch Browsersync and watch JS files
gulp.task('default', gulp.series('css', function () {

    // Serve files from the root of this project

  

  
    // add browserSync.reload to the tasks array to make
    // all browsers reload after tasks are complete.
    gulp.watch("*.css",gulp.parallel ('css'));
}));

