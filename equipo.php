<?php get_header();

/*
Template Name: Equipo
Template Post Type: post, page, event
*/
// 

?>
<style>
	h3 {
		margin-top: 50px;
	}
</style>

<div class="container mx-auto px-4 md:px-8 xl:px-40"  style="min-height: 60vh">

<?php
// WP_Query arguments
$args = array(
	'post_type'              => array( 'equipo' ),
	'nopaging'               => false,
	'posts_per_page'         => '-1',
	'posts_per_archive_page' => '-1',
	'ignore_sticky_posts'    => true,
	'order'                  => 'ASC',
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) { 
		$query->the_post(); 
	} 
}
?>
	
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<header class="header">
			<h1 class="text-center text-5xl mb-10 tracking-tight font-bold "><?php the_title(); ?></h1> 
		</header>
		
		<div class="entry-content">
			<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
			<?php the_content(); ?>
			
		</div>
	
	</article>
    
	


<?php wp_reset_postdata();?>
	
	
	


</div>


<?php edit_post_link(); ?>


<?php //get_sidebar(); ?>
<?php get_footer(); ?>