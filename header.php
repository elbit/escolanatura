<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width" />
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i" rel="stylesheet">
		
		<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.css"> -->
		
		<link href="<?php echo get_template_directory_uri(); ?>/build/style-min.css" rel="stylesheet">
		
		<script src="<?php echo get_template_directory_uri(); ?>/js/uikit.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/fitty@2.2.6/dist/fitty.min.js"></script>
		
		<script defer src="https://use.fontawesome.com/releases/v5.8.0/js/all.js" integrity="sha384-ukiibbYjFS/1dhODSWD+PrZ6+CGCgf8VbyUH7bQQNUulL+2r59uGYToovytTf4Xm" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
		<script src="https://cdn.jsdelivr.net/npm/wowjs@1.1.3/dist/wow.min.js"></script>
		
		<script>
		new WOW().init();
		</script>
		
		<?php wp_head(); ?>
		<!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','https://connect.facebook.net/en_US/fbevents.js'); fbq('init', '1192378530886756'); fbq('track', 'PageView');fbq('track', 'PageView');fbq('track', 'Contact');fbq('track', 'FindLocation');fbq('track', 'SubmitApplication');</script><noscript> <img height="1" width="1" src="https://www.facebook.com/tr?id=1192378530886756&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->
	</head>
	
	<body <?php body_class('font-sans leading-normal text-grey-darkest antialiased font-normal'); ?>>
		
		
		<?php if ( is_front_page() ) : ?>
		<picture class="absolute top-0">
			<source media="(max-width: 800px)" srcset="<?php echo get_template_directory_uri(); ?>/img/hero-1.jpg">
			<source media="(min-width: 1200px)" srcset="<?php echo get_template_directory_uri(); ?>/img/hero-4.jpg">
			<source media="(max-width: 1200px)" srcset="<?php echo get_template_directory_uri(); ?>/img/hero-3.jpg">
			<img src="<?php echo get_template_directory_uri(); ?>/img/hero-4.jpg" alt="Flowers" >
			
		</picture>
		
		<?php endif; ?>
		
		<header id="header" class="container container-xl mx-auto " >
			
			<div class="flex flex-col items-center">
				
				<?php if ( is_front_page() ) : ?>
				
				<a class="p-10 absolute top-0 max-w-1/2 sm:max-w-full" href="<?php echo get_home_url(); ?>">
					<img class=" wow fadeInDown" src="<?php echo get_template_directory_uri(); ?>/img/logo-color-round.svg" alt="" "
					ukx-parallax="y:0,20" >
				</a>
				
				<?php else: ?>
				
				<a class="hover:opacity-75 p-1 max-w-1/2 sm:max-w-full" href="<?php echo get_home_url(); ?>">
					<img class=" wow fadeInDown" src="<?php echo get_template_directory_uri(); ?>/img/logo-color-round.svg" alt="" >
				</a>
				
				<?php endif; ?>
			</div>
			
		</header>






