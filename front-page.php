<?php get_header(); ?>

<div class="container mx-auto md:px-8 xl:px-10">

	<!-- <div class="section  p-15 bg-red-light font-bold text-xl text-center z-50" uk-parallax="y: 0, 40" >
	Per motius logístics i organitzatius ens hem vist obligats a cancel·lar la sessió de portes obertes del dimecres 24. <br> La visita es podrà realitzar el dissabte 27 pel matí.
	</div> -->

	<div class="section hero bg-yellow-400 z-50 " uk-parallax="y: 0, 40" style="margin-top: 60vh">
		<!-- <ul>
			<?php pll_the_languages(); ?>
		</ul> -->
		<h1 id="fittyk" class="hero-title text-3xl lg:text-5xl "> <?php pll_e('Nou projecte educatiu al Baix Llobregat') ?></h1>
		
		<h2 id="fitty-sb" class="hero-subtitle text-xl lg:text-2xl"><?php pll_e('Aprenentatge significatiu trilingüe dels 3 als 12 anys <br>
			Molins de Rei · Parc Natural del Collserola') ?></h2>
	</div>

	<div class="section bg-green-400 flex flex-wrap text-white"
		uk-parallax="y: 0, 10">
		
		<div class="w-full lg:w-1/2 ">
			<div class="p-1 md:p-4">
				<img src="<?php echo get_template_directory_uri(); ?>/img/masia-slim.jpg" alt="">
			</div>
		</div>
		
		<div class="w-full lg:w-1/2 ">
			<div class="p-1 md:p-4">
				<p class="text-"><?php pll_e('Un centre educatiu trilingüe per a edats d\'entre 3 fins a 12 anys, en una masia al bosc de Molins de Rei, dins del Parc Natural de Collserola. Natura és un projecte educatiu amb una metodologia educativa basada majoritàriament en la <strong>pedagogia Montessori</strong>, amb ràtios reduïdes, cuina pròpia vegetariana, aiurvèdica i ecològica, activitats complementàries i amb transport organitzat (a concretar en funció de les necessitats) des de Sant Feliu de Llobregat i Molins de Rei.') ?></p>
			</div>
		</div>
		
		<div class="w-full lg:w-1/2 ">
			<div class="p-1 md:p-4">
				<p class="text-"><?php pll_e('Natura està ubicada en una preciosa masia del segle XIV totalment restaurada, amb 12 hectàrees de zona verda en ple bosc, horts, basses i esplanades que ofereixen ambients rics i diversos per interactuar i aprendre.<br>
				Situada prop de l\'antic camí de Santa Creu d\'Olorda, en ple Parc Natural de Collserola, es troba en un mirador al Baix Llobregat amb vistes des del mar i a la muntanya de Montserrat. A 10 minuts en cotxe des del centre de Molins de Rei i 15 de Sant Feliu de Llobregat') ?></p>
			</div>
		</div>
			
		<div class="w-full lg:w-1/2 ">
			<div class="p-1 md:p-4">
					<img src="<?php echo get_template_directory_uri(); ?>/img/natura-cenital.jpg" alt="">
			</div>
		</div>

		<div class="w-full">

			<h2 class="text-3xl text-bold pb-3  pt-5 text-center"><strong>Més informació: </strong></h2>
		</div>
		<div class="flex flex-wrap w-full justify-between  md:p-4">

			<a href="http://naturaescolaviva.com/els-nostres-espais/" 

			class="w-1/2 lg:w-1/4 p-1 lg:p-4 btn-yellow pt-2
			flex flex-middle justify-center items-center flex-wrap flex-col text-center mt-5" >
				<i class="far fa-images fa-2x text-white "> </i> <br>
				<span class="text-center text-lg lg:text-2xl">Visita la galeria d'imatges</span>
			</a>

			<a href="http://naturaescolaviva.com/equipo/"

			class="w-1/2 lg:w-1/4 p-1 lg:p-4 btn-green bg-red-400 flex  justify-center items-center flex-wrap flex-col text-center mt-5 pt-2" >
				<i class="fas fa-users fa-2x text-white "> </i> <br>
				<span class="text-center text-lg lg:text-2xl">Coneix el nostre equip</span>
			</a>

			<a href="http://naturaescolaviva.com/bases-pedagogicas/" 

			class="w-1/2 lg:w-1/4 p-1 lg:p-4 btn-green bg-green-500 flex justify-center items-center flex-wrap flex-col text-center mt-5 pt-2" >
				<i class="fas fa-graduation-cap fa-2x text-white "> </i> <br>
				<span class="text-center text-lg lg:text-2xl">Bases Pedagògiques</span>
			</a>
			
			<a href="http://naturaescolaviva.com/un-dia-en-la-escuela/" 

			class="w-1/2 lg:w-1/4 p-1 lg:p-4 btn-green bg-brown-500 flex justify-center items-center flex-wrap flex-col text-center mt-5 pt-2" style="background-color: #BE985D;">
				<i class="fas fa-sun fa-2x text-white "> </i> <br>
				<span class="text-center text-lg lg:text-2xl">Un dia a l'escola</span>
			</a>

			<a href="http://naturaescolaviva.com/matriculacion" class="w-full  p-1 lg:p-4 btn-yellow  flex justify-around items-center flex-wrap text-center mt-5 pt-2 text-gray-700 text-2xl" style="background-color: white;">
				<i class="fas fa-file-signature fa-2x"></i>
				<span class="text-center text-xl lg:text-4xl text-gray-700">MATRICULA OBERTA TOT L'ANY </span><i class="fas fa-arrow-circle-right fa-2x"></i>
			</a>

		</div>
	 
		
		<div class="flex w-full p-1 md:p-4 flex-wrap">
			
			<div class="w-full md:w-1/3 pt-4">
				<img src="<?php echo get_template_directory_uri(); ?>/img/banner-1.jpg" alt="">
			</div>
			<div class="w-full md:w-1/3 pt-4">
				<img src="<?php echo get_template_directory_uri(); ?>/img/banner-3.jpg" alt="">
			</div>
			<div class="w-full md:w-1/3 pt-4">
				<img src="<?php echo get_template_directory_uri(); ?>/img/banner-2.jpg" alt="">
			</div>
		</div>

		
	</div>
	
	
	<div class="section flex content-between bg-red-400 flex-middle flex-wrap" >
		
		<div class="w-full md:w-1/2 ">
			<div class="p-1 md:p-4">
				<p class="text-3xl font-bold tracking-tight leading-tight text-center text-white " ><?php pll_e('
Vols coneixer el projecte?') ?></p>
			</div>
			
		</div>
		
		<div class="w-full md:w-1/2  ">
			<div class="p-1 md:p-4 text-center">
				
				<a href="http://naturaescolaviva.com/cita-aula-pilot/" class="inline-block bg-green-500 p-4 text-white  text-xl text-center no-underline rounded  hover:bg-green-600 w-full align-middle uppercase font-bold wow bounceInUp flex items-center justify-around" >
					<i class="fas fa-calendar-alt fa-2x text-white"> </i> <?php pll_e('  Demanar cita ')?> 
				<i class="fas fa-arrow-right fa text-white"> </i> </a>
			</div>
		</div>
	</div>
	
	<div class="section-full flex content-between leading-none flex-wrap">
		
		<div class="w-full sm:w-1/2 xl:w-1/4 p-1 md:p-4 mb-3 md:mb-1">
			
			<a  href="tel:696379392" class="btn btn-yellow btn-full">
				<i class="fas fa-phone"></i> 696 37 93 92 * <br>
			</a>
			<div class="text-center mt-2"><small>*16h-19h de dilluns a divendres</small></div>
		</div>

		<div class="w-full sm:w-1/2 xl:w-1/4 p-1 md:p-4 mb-3 md:mb-1">
			<a href="https://www.facebook.com/naturaescolaviva/" target="_blank" class=" btn btn-blue btn-full"><i class="fab fa-facebook-square"></i> Facebook</a>
		</div>
		
		<div class="w-full sm:w-1/2 xl:w-1/4 p-1 md:p-4 mb-3 md:mb-1">
			<a href="mailto:info@naturaescolaviva.com" class=" btn btn-green btn-full "><i class="fas fa-envelope"></i> info@naturaescolaviva.com</a>
		</div>
		
		<div class="w-full sm:w-1/2 xl:w-1/4 p-1 md:p-4 mb-3 md:mb-1">
			<a href="/com-arribar" target="_blank"class="btn btn-red btn-full "><i class="fas fa-map-signs"></i> <?php pll_e('Com arribar') ?></a>
		</div>
	</div>

	<div class="section flex content-around bg-red-400 flex-middle flex-wrap" style="justify-content: space-around;">
		
		<div class="sm:w-1/2">
		<img src="<?php echo get_template_directory_uri(); ?>/img/banner-16-1.jpg" alt="" class="mb-4">
		</div>
		<div class="sm:w-1/2">
		<img src="<?php echo get_template_directory_uri(); ?>/img/banner-16-2.jpg" alt="" class="mb-4">
		</div>
		<div class="sm:w-1/2">
		<img src="<?php echo get_template_directory_uri(); ?>/img/banner-16-3.jpg" alt="" class="">
		</div>
		<div class="sm:w-1/2">
		<img src="<?php echo get_template_directory_uri(); ?>/img/banner-16-4.jpg" alt="" class="mb">
	</div>
		


	</div>

	
	
</div>


<style>
	#fitty {
display: inline-block;
white-space: nowrap;
}
</style>

<script>
	fitty('#fitty', {
	minSize: 40,
	maxSize: 50
	});
	fitty('#fitty-sub', {
	minSize: 25,
	maxSize: 32
	});
</script>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>